package com.feeko256.task9

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import java.util.Random


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    public fun randomizeButtonClick(view: View) {
        val imgView = findViewById<ImageView>(R.id.imageView)
        val button = findViewById<Button>(R.id.button)
        when((0..6).random()){
            0-> imgView.setImageResource(R.drawable.p1)
            1-> imgView.setImageResource(R.drawable.p2)
            2-> imgView.setImageResource(R.drawable.p3)
            3-> imgView.setImageResource(R.drawable.p4)
            4-> imgView.setImageResource(R.drawable.p5)
            5-> imgView.setImageResource(R.drawable.p6)
            6-> imgView.setImageResource(R.drawable.p7)
        }
        button.setBackgroundColor(Colors.entries.shuffled().first().rgb)
    }
}
enum class Colors(val rgb: Int){
    GREEN(Color.GREEN),
    YELLOW(Color.YELLOW),
    LTGRAY(Color.LTGRAY),
    CYAN(Color.CYAN),
    WHITE(Color.WHITE),
}